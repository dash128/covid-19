package com.example.covid19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.example.covid19.model.Alerta
import com.example.covid19.model.Lugar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_opcion3.*
import kotlin.math.log

class Opcion3Activity : AppCompatActivity(), View.OnClickListener {
    val db = FirebaseFirestore.getInstance()
    val listLugar: Array<Lugar>? = null
    lateinit var txt_nombre:EditText
    lateinit var txt_apellido:EditText
    lateinit var txt_descripcion:EditText
    lateinit var txt_lugar:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opcion3)

        txt_nombre = findViewById(R.id.txt_nombre) as EditText
        txt_apellido = findViewById(R.id.txt_apellido) as EditText
        txt_descripcion = findViewById(R.id.txt_descripcion) as EditText
        txt_lugar = findViewById(R.id.txt_lugar) as EditText

        btn_guardar_alerta.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btn_guardar_alerta -> {
                createAlerta()
            }
        }
    }

    fun createAlerta(){
        val alertaRef = db.collection("alertas")
        val idAlerta = alertaRef.document().id
        val newAlerta = Alerta(idAlerta,txt_nombre.text.toString(),txt_apellido.text.toString(),txt_descripcion.text.toString(),txt_lugar.text.toString());
        alertaRef.document(idAlerta).set(newAlerta)
        Toast.makeText(this, "Guardado exitosamente", Toast.LENGTH_SHORT).show()
        this.onBackPressed()
    }

}