package com.example.covid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        opcion_1.setOnClickListener(this)
        opcion_2.setOnClickListener(this)
        opcion_3.setOnClickListener(this)
        opcion_4.setOnClickListener(this)
        opcion_5.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.opcion_1 -> {
                startActivity( Intent(this, Opcion1Activity::class.java))
            }
            R.id.opcion_2 -> {
                startActivity( Intent(this, Opcion2Activity::class.java))
            }
            R.id.opcion_3 -> {
                startActivity( Intent(this, Opcion3Activity::class.java))
            }
            R.id.opcion_4 -> {
                startActivity( Intent(this, Opcion4Activity::class.java))
            }
            R.id.opcion_5 -> {
                startActivity( Intent(this, Opcion5Activity::class.java))
        }
        }
    }
}