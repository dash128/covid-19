package com.example.covid19.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19.R;
import com.example.covid19.model.Alerta;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

import org.jetbrains.annotations.NotNull;

import kotlin.Unit;

public class alertaAdapter extends FirestoreRecyclerAdapter<Alerta, alertaAdapter.MyViewHolder> {
    private OnItemClickListener listener;

    public alertaAdapter(@NonNull FirestoreRecyclerOptions<Alerta> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i, @NonNull Alerta alerta) {
        myViewHolder.txt_alerta_nombre.setText(alerta.getNombre());
        Log.d("dash","->"+alerta.getNombre().toString());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alerta, parent, false);
        return new MyViewHolder(v);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_alerta_nombre;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_alerta_nombre = itemView.findViewById(R.id.rv_txt_nombre);


            itemView.setOnClickListener(new View.OnClickListener(){


                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    //startActivity( Intent(this, Opcion5Activity::class.java))

                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                    //Toast.makeText(v.getContext(),"asdasd",Toast.LENGTH_SHORT).show();

                }
            });
        }
    }
}
