package com.example.covid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.covid19.adapter.alertaAdapter
import com.example.covid19.model.Alerta
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query


class Opcion5Activity : AppCompatActivity() {
    lateinit var recycler_reporte: RecyclerView
    lateinit var adapter: alertaAdapter
    lateinit var db: FirebaseFirestore
    var arrAlerta = arrayListOf<Alerta>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opcion5)

        recycler_reporte = findViewById(R.id.recycler_reporte)
        recycler_reporte.layoutManager = LinearLayoutManager(this)
        //recycler_reporte.adapter = alertaAdapter()
        db = FirebaseFirestore.getInstance()

        val query = db.collection("alertas")
        val response = FirestoreRecyclerOptions.Builder<Alerta>().setQuery(query, Alerta::class.java).build()

        adapter = alertaAdapter(response)
        adapter.notifyDataSetChanged()
        recycler_reporte.adapter = adapter


        adapter.setOnItemClickListener(alertaAdapter.OnItemClickListener { documentSnapshot, position ->
            //Toast.makeText(this,"sd-> "+position +" y"+ documentSnapshot.data.toString(),Toast.LENGTH_SHORT).show();
            val intent = Intent(this, DetalleAlertaActivity::class.java )
            intent.putExtra("alerta",documentSnapshot.data?.get("alerta").toString())
            intent.putExtra("apellido",documentSnapshot.data?.get("apellido").toString())
            intent.putExtra("lugar",documentSnapshot.data?.get("lugar").toString())
            intent.putExtra("nombre",documentSnapshot.data?.get("nombre").toString())

            startActivity(intent)
        })
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }
}