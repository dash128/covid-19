package com.example.covid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class DetalleAlertaActivity : AppCompatActivity() {
    lateinit var data_nombre: TextView
    lateinit var data_lugar: TextView
    lateinit var data_alerta: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_alerta)

        data_nombre = findViewById(R.id.data_nombre) as TextView
        data_lugar = findViewById(R.id.data_lugar) as TextView
        data_alerta = findViewById(R.id.data_alerta) as TextView

        val intent: Intent = getIntent()

        data_nombre.text = intent.getStringExtra("nombre") + " " + intent.getStringExtra("apellido")
        data_lugar.text = intent.getStringExtra("lugar")
        data_alerta.text = intent.getStringExtra("alerta")

        //Toast.makeText(this,"F-> "+intent.getStringExtra("alerta"),Toast.LENGTH_SHORT).show()
    }
}