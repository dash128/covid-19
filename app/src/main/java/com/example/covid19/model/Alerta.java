package com.example.covid19.model;

public class Alerta {
    private String id;
    private String nombre;
    private String apellido;
    private String alerta;
    private String lugar;

    Alerta(){}

    public Alerta(String id, String nombre, String apellido, String alerta, String lugar) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.alerta = alerta;
        this.lugar = lugar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getAlerta() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta = alerta;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
}
