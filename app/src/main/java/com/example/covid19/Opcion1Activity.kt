package com.example.covid19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.covid19.model.Alerta
import com.google.firebase.firestore.FirebaseFirestore

class Opcion1Activity : AppCompatActivity() {
    val db = FirebaseFirestore.getInstance()
    lateinit var txt_fallecidos: TextView
    lateinit var txt_confirmados: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opcion1)

        txt_fallecidos = findViewById(R.id.covid_fallecidos) as TextView
        txt_confirmados = findViewById(R.id.covid_confirmados) as TextView

        getDatosCovid()


    }

    private fun getDatosCovid() {
        // [START get_document]
        val docRef = db.collection("datos").document("covid")
        /*docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("TAG", "DocumentSnapshot data: ${document.data?.get("fallecidos").toString()}")
                } else {
                    Log.d("TAG", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("TAG", "get failed with ", exception)
            }*/

        docRef.addSnapshotListener{ snapshot, exception ->
            if (exception != null) {
                Log.d("TAG", "error: ${exception.message}")
                return@addSnapshotListener
            } else {
                txt_confirmados.text = snapshot?.data?.get("confirmados").toString()
                txt_fallecidos.text = snapshot?.data?.get("fallecidos").toString()

                //Log.d("TAG", "No such document"+snapshot?.data?.get("fallecidos").toString())
            }
        }
    }
}