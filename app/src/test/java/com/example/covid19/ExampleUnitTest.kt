package com.example.covid19

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun registrar_isCorrect(user){
        assertEquals(true, createUser(user))
    }

    @Test
    fun recordar_contraseña_isCorrect(constraseña, login){
        assertEquals(true, verificarPAssword(password, login))
    }

}